package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	chatModels "gitlab.com/D.Koltsov/rsoi-chats/models"
	userModels "gitlab.com/D.Koltsov/rsoi-users/models"
	"gitlab.com/D.Koltsov/rsoigram/lib"
)

const jsonType = "application/json"

// GetUsers routes to users service
func GetUsers(c *gin.Context) {
	// TODO: support query params
	resp, err := http.Get(lib.Cfg.UsersURL + "/users")
	if err != nil {
		log.Printf("err while getting users: %s", err)
		c.AbortWithStatus(http.StatusFailedDependency)
		return
	}

	var response interface{}
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		log.Printf("err while getting users: %s", err)
		c.AbortWithStatus(500)
		return
	}

	c.JSON(http.StatusOK, response)
}

// GetUser routes to users service
func GetUser(c *gin.Context) {
	// TODO: support query params
	id := c.Params.ByName("id")
	resp, err := http.Get(lib.Cfg.UsersURL + "/users/" + id)
	if err != nil {
		log.Printf("err while getting user: %s", err)
		c.AbortWithStatus(http.StatusFailedDependency)
		return
	}

	var response interface{}
	err = json.NewDecoder(resp.Body).Decode(&response)
	if err != nil {
		log.Printf("err while getting user: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, response)
}

// GetUser routes to users service
func GetUserByLogin(c *gin.Context) {
	// TODO: support query params
	login, ok := c.GetQuery("login")
	if !ok {
		c.AbortWithStatus(http.StatusBadRequest)
	}

	resp, err := http.Get(lib.Cfg.UsersURL + "/users?q[login]=" + login)
	if err != nil {
		log.Printf("err while getting user: %s", err)
		c.AbortWithStatus(http.StatusFailedDependency)
		return
	}

	var users []userModels.User
	err = json.NewDecoder(resp.Body).Decode(&users)
	if err != nil {
		log.Printf("err while getting user: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	if len(users) == 0 {
		c.AbortWithStatus(400)
		return
	}

	c.JSON(http.StatusOK, users[0])
}

// func CreateMessage(c *gin.Context) {
// }

// func UpdateMessage(c *gin.Context) {
// }

type deleteUserReq struct {
	ID uint `json:"id,omitempty"`
}

func DeleteUser(c *gin.Context) {
	var req deleteUserReq
	err := json.NewDecoder(c.Request.Body).Decode(&req)
	if err != nil {
		c.AbortWithStatus(400)
		return
	}
	userID := strconv.Itoa(int(req.ID))

	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	deleteUserURL := lib.Cfg.UsersURL + "/users/" + userID
	usersReq, _ := http.NewRequest("DELETE", deleteUserURL, nil)
	_, err = client.Do(usersReq)
	if err != nil {
		log.Printf("failed to delete user: %s", err)
		c.AbortWithStatus(http.StatusFailedDependency)
		return
	}

	userChatsResp, err := http.Get(lib.Cfg.ChatsURL + "/user_chats?q[user_id]=" + userID)
	if err != nil {
		log.Printf("failed to get user chats: %s", err)
		c.AbortWithStatus(http.StatusFailedDependency)
		return
	}
	var userChats []chatModels.UserChat
	err = json.NewDecoder(userChatsResp.Body).Decode(&userChats)
	if err != nil {
		log.Printf("failed to get user chats: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	for _, us := range userChats {
		usID := strconv.Itoa(int(us.ID))
		delReq, _ := http.NewRequest("DELETE", lib.Cfg.ChatsURL+"/user_chats/"+usID, nil)
		_, err := client.Do(delReq)
		if err != nil {
			log.Printf("failed to delete user chats: %s", err)
			c.AbortWithStatus(http.StatusFailedDependency)
			return
		}
	}
}
