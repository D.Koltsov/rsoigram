package controllers

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	chatModels "gitlab.com/D.Koltsov/rsoi-chats/models"
	models "gitlab.com/D.Koltsov/rsoi-messages/models"
	"gitlab.com/D.Koltsov/rsoigram/lib"
)

// func GetMessages(c *gin.Context) {
// 	http.Get(lib.Cfg.MsgURL+"/")
// }

// func GetMessage(c *gin.Context) {
// }

type createMessageReq struct {
	Text     string `json:"text,omitempty"`
	ChatID   uint   `json:"chat_id,omitempty"`
	SenderID uint   `json:"sender_id,omitempty"`
}

func CreateMessage(c *gin.Context) {
	var req createMessageReq
	err := json.NewDecoder(c.Request.Body).Decode(&req)
	if err != nil {
		c.AbortWithStatus(400)
		return
	}

	now := time.Now()
	newMessage := models.Message{
		Text:      req.Text,
		ChatID:    req.ChatID,
		SenderID:  req.SenderID,
		CreatedAt: &now,
		UpdatedAt: &now,
	}
	messageData, err := json.Marshal(newMessage)
	if err != nil {
		log.Printf("failed to create message: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	msgResp, err := http.Post(lib.Cfg.MsgURL+"/messages", jsonType, bytes.NewReader(messageData))
	if err != nil {
		log.Printf("failed to create message: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	var msg models.Message
	err = json.NewDecoder(msgResp.Body).Decode(&msg)
	if err != nil {
		log.Printf("failed to create msg: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	chatUpd := chatModels.Chat{
		LastMessageID: msg.ID,
		UpdatedAt:     &now,
	}
	body, err := json.Marshal(chatUpd)
	if err != nil {
		log.Printf("failed to create msg: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	chatID := strconv.Itoa(int(req.ChatID))
	chatReq, err := http.NewRequest("POST", lib.Cfg.ChatsURL+"/chats/"+chatID, bytes.NewReader(body))
	if err != nil {
		log.Printf("failed to create msg: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	client := http.Client{
		Timeout: 5 * time.Second,
	}
	resp, err := client.Do(chatReq)
	if err != nil {
		log.Printf("failed to create msg: %s", err)
		// c.AbortWithStatus(http.StatusFailedDependency)
		lib.Cfg.DefaultQueue <- lib.Retry{Count: 5, Req: *chatReq}
		return
	}
	if resp.StatusCode != 200 {
		log.Printf("failed to create msg: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
}

type reqMessagesByChat struct {
	ChatID int `json:"chat_id,omitempty"`
}

func GetMessagesByChatID(c *gin.Context) {
	var req reqMessagesByChat
	err := json.NewDecoder(c.Request.Body).Decode(&req)
	if err != nil {
		c.AbortWithStatus(400)
		return
	}

	msgsResp, err := http.Get(lib.Cfg.MsgURL + "/messages?q[chat_id]=" + strconv.Itoa(req.ChatID))
	if err != nil {
		log.Printf("failed to get msgs: %s", err)
		c.AbortWithStatus(http.StatusFailedDependency)
		return
	}

	var msgs []models.Message
	err = json.NewDecoder(msgsResp.Body).Decode(&msgs)
	if err != nil {
		log.Printf("failed to get last message: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, msgs)
}

// func UpdateMessage(c *gin.Context) {
// }

// func DeleteMessage(c *gin.Context) {
// }
