package controllers

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/D.Koltsov/rsoigram/lib"
)

type reqLogin struct {
	Login    string `json:"login,omitempty"`
	Password string `json:"password,omitempty"`
}

func Login(c *gin.Context) {
	var req reqLogin
	err := json.NewDecoder(c.Request.Body).Decode(&req)
	if err != nil {
		c.AbortWithStatus(400)
		return
	}

	body, err := json.Marshal(req)
	if err != nil {
		log.Printf("failed to marshall login: %s", err)
		c.AbortWithStatus(500)
		return
	}
	loginReq, _ := http.NewRequest("POST", lib.Cfg.AuthURL+"/login", bytes.NewReader(body))
	client := http.Client{
		Timeout: 5 * time.Second,
	}
	resp, err := client.Do(loginReq)
	if err != nil {
		log.Printf("failed to login user")
		c.AbortWithStatus(http.StatusFailedDependency)
		return
	}
	if resp.StatusCode != http.StatusOK {
		c.AbortWithStatus(http.StatusForbidden)
		return
	}

	type authResponse struct {
		Token string `json:"token"`
	}
	var auth authResponse
	err = json.NewDecoder(resp.Body).Decode(&auth)
	if err != nil {
		log.Printf("failed to login: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, auth)
}
