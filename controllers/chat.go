package controllers

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	models "gitlab.com/D.Koltsov/rsoi-chats/models"
	msgModels "gitlab.com/D.Koltsov/rsoi-messages/models"
	"gitlab.com/D.Koltsov/rsoigram/lib"
)

// ChatInfo is a response to chats info
type ChatInfo struct {
	ID            uint              `json:"id"`
	LastMessageID uint              `json:"last_message_id"`
	UpdatedAt     *time.Time        `json:"updated_at"`
	Users         []models.UserChat `json:"users,omitempty"`
}

// GetChat routes data to chat and user services
func GetChat(c *gin.Context) {
	chatID := c.Params.ByName("id")

	userChatsURL := lib.Cfg.ChatsURL + "/user_chats"
	req, _ := http.NewRequest("GET", userChatsURL, nil)
	query := req.URL.Query()
	query.Add("q[chat_id]", chatID)
	req.URL.RawQuery = query.Encode()

	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	userChatsResp, err := client.Do(req)
	if err != nil {
		log.Printf("failed to get user chats: %s", err)
		c.AbortWithStatus(http.StatusFailedDependency)
		return
	}
	var users []models.UserChat
	err = json.NewDecoder(userChatsResp.Body).Decode(&users)
	if err != nil {
		log.Printf("failed to get user chats: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	chatResp, err := http.Get(lib.Cfg.ChatsURL + "/chats/" + chatID)
	if err != nil {
		log.Printf("failed to get chat: %s", err)
		c.AbortWithStatus(http.StatusFailedDependency)
		return
	}
	var chat models.Chat
	err = json.NewDecoder(chatResp.Body).Decode(&chat)
	if err != nil {
		log.Printf("failed to get chat: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	resp := ChatInfo{
		ID:            chat.ID,
		LastMessageID: chat.LastMessageID,
		UpdatedAt:     chat.UpdatedAt,
		Users:         users,
	}

	c.JSON(http.StatusOK, resp)
}

type createChatReq struct {
	UserIDs []uint `json:"user_ids,omitempty"`
}

// CreateChat routes data to chat service
func CreateChat(c *gin.Context) {
	var req createChatReq
	err := json.NewDecoder(c.Request.Body).Decode(&req)
	if err != nil {
		c.AbortWithStatus(400)
		return
	}

	now := time.Now()
	newChat := models.Chat{
		LastMessageID: 0,
		CreatedAt:     &now,
		UpdatedAt:     &now,
	}
	chatData, err := json.Marshal(newChat)
	if err != nil {
		log.Printf("failed to create chat: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	resp, err := http.Post(lib.Cfg.ChatsURL+"/chats", jsonType, bytes.NewReader(chatData))
	if err != nil {
		log.Printf("failed to create chat: %s", err)
		c.AbortWithStatus(http.StatusFailedDependency)
		return
	}
	var chat models.Chat
	err = json.NewDecoder(resp.Body).Decode(&chat)
	if err != nil {
		log.Printf("failed to create chat: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	for _, uid := range req.UserIDs {
		err := createUserChat(chat.ID, uid)
		if err != nil {
			req, _ := http.NewRequest("DELETE", lib.Cfg.ChatsURL+"/chats/"+strconv.Itoa(int(chat.ID)), nil)
			client := &http.Client{
				Timeout: time.Second * 5,
			}
			if _, err := client.Do(req); err != nil {
				c.AbortWithError(http.StatusInternalServerError, err)
				return
			}
		}
	}
}

func createUserChat(id uint, uid uint) error {
	now := time.Now()

	userChat := models.UserChat{
		ChatID:    id,
		UserID:    uid,
		UpdatedAt: &now,
		CreatedAt: &now,
	}
	data, err := json.Marshal(userChat)
	if err != nil {
		log.Printf("failed to create chat: %s", err)
		return err
	}

	_, err = http.Post(lib.Cfg.ChatsURL+"/user_chats", jsonType, bytes.NewReader(data))
	if err != nil {
		log.Printf("failed to create user_chat: %s", err)
		return err
	}
	return nil
}

type reqListChat struct {
	Limit int `json:"limit"`
	Page  int `json:"page"`
}

type ChatsList struct {
	Chats    []models.Chat       `json:"chats,omitempty"`
	Messages []msgModels.Message `json:"messages,omitempty"`
}

func ListChats(c *gin.Context) {
	var req reqListChat
	err := json.NewDecoder(c.Request.Body).Decode(&req)
	if err != nil {
		c.AbortWithStatus(400)
		return
	}
	limit := strconv.Itoa(req.Limit)
	page := strconv.Itoa(req.Page)

	chatResp, err := http.Get(lib.Cfg.ChatsURL + "/chats?limit=" + limit + "&page=" + page)
	if err != nil {
		log.Printf("failed to get chats: %s", err)
		c.AbortWithStatus(http.StatusFailedDependency)
		return
	}
	var chats []models.Chat
	err = json.NewDecoder(chatResp.Body).Decode(&chats)
	if err != nil {
		log.Printf("failed to get chat: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}

	var msgs []msgModels.Message
	for _, chat := range chats {
		if chat.LastMessageID == 0 {
			continue
		}
		msgID := strconv.Itoa(int(chat.LastMessageID))
		msgResp, err := http.Get(lib.Cfg.MsgURL + "/messages/" + msgID)
		if err != nil {
			continue
			// log.Printf("failed to get last message: %s", err)
			// c.AbortWithStatus(http.StatusInternalServerError)
			// return
		}
		var msg msgModels.Message
		err = json.NewDecoder(msgResp.Body).Decode(&msg)
		if err != nil {
			log.Printf("failed to get last message: %s", err)
			c.AbortWithStatus(http.StatusInternalServerError)
			return
		}
		msgs = append(msgs, msg)
	}

	list := ChatsList{
		Chats:    chats,
		Messages: msgs,
	}

	c.JSON(http.StatusOK, list)
}
