package auth

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"

	sessions "gitlab.com/D.Koltsov/rsoigram/sessions"

	log "github.com/sirupsen/logrus"
	tarantool "github.com/tarantool/go-tarantool"
)

// AuthMiddleware ...
type AuthMiddleware struct {
	sessionStore *sessions.SessionStore
}

// NewAuthMiddleware ...
func NewAuthMiddleware(tnt *tarantool.Connection) *AuthMiddleware {
	return &AuthMiddleware{sessionStore: sessions.NewSessionStore(tnt)}
}

const bearer = "Bearer "

// Handle ...
func (m *AuthMiddleware) Auth(c *gin.Context) {
	var sid string
	if qsid := c.Query("sid"); qsid != "" {
		sid = qsid
	} else if hsid := strings.TrimPrefix(c.GetHeader("Authorization"), "Bearer "); hsid != "" {
		sid = hsid
	}

	// current len(sid) == 32, so check something in the middle and around 32
	if len(sid) < 24 || len(sid) > 64 {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	_, err := m.sessionStore.GetUserID(sessions.SessionID(sid))
	if err == sessions.ErrNotFound {
		log.Printf("User sid: %s not found in session store", sid)
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	if err != nil {
		log.Printf("failed to get user id: %s", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	c.Next()
}
