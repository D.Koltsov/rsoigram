package auth

import "context"

type ctxUserIDKey struct{}

// CtxWithUserID ...
func CtxWithUID(ctx context.Context, uid string) context.Context {
	return context.WithValue(ctx, ctxUserIDKey{}, uid)
}

// UserIDFromCtx ...
func UIDFromCtx(ctx context.Context) string {
	uid, _ := ctx.Value(ctxUserIDKey{}).(string)
	return uid
}
