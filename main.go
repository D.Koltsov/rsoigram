package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"sync/atomic"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/D.Koltsov/rsoigram/lib"
	"gitlab.com/D.Koltsov/rsoigram/lib/terminator"
	"gitlab.com/D.Koltsov/rsoigram/server"
)

func main() {
	// Init default flags
	fs := flag.NewFlagSet("go-service-boilerplate-fs", flag.ContinueOnError)
	lib.InitConfig(fs, &lib.Cfg)

	if err := fs.Parse(os.Args[1:]); err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(1)
	}

	lib.Cfg.DefaultQueue = make(chan lib.Retry)
	go func() {
		for {
			r, ok := <-lib.Cfg.DefaultQueue
			if !ok {
				continue
			}

			client := &http.Client{
				Timeout: time.Second * 5,
			}
			_, err := client.Do(&r.Req)
			log.Printf("retried request")
			if err != nil {
				log.Printf("err while retrying req %s", err)
				log.Println("retry next time")
				atomic.AddInt32(&r.Count, -1)
				if r.Count > 0 {
					go func() {
						lib.Cfg.DefaultQueue <- r
					}()
				}
			}
		}
	}()

	s := server.Setup()
	port := "3000"

	if p := os.Getenv("PORT"); p != "" {
		if _, err := strconv.Atoi(p); err == nil {
			port = p
		}
	}
	s.Run(":" + port)

	sigHandler := terminator.TermSignal()
	ctx := context.Background()
	err := sigHandler.Wait(ctx)
	if err != nil && err != terminator.ErrTermSig {
		log.WithFields(log.Fields{
			"error": err,
		}).Fatal("failed to caught signal")
	}
}
