package sessions

import (
	"context"
	"errors"
	"fmt"

	tarantool "github.com/tarantool/go-tarantool"
)

var ErrNotFound = errors.New("record not found")

type Session struct {
	SessionID SessionID
}

// SessionID ...
type SessionID string

type ctxSessionIDKey struct{}

// CtxWithSID ...
func CtxWithSID(ctx context.Context, sid SessionID) context.Context {
	return context.WithValue(ctx, ctxSessionIDKey{}, sid)
}

// SIDFromCtx ...
func SIDFromCtx(ctx context.Context) SessionID {
	uid, _ := ctx.Value(ctxSessionIDKey{}).(SessionID)
	return uid
}

// SessionStore ...
type SessionStore struct {
	tnt *tarantool.Connection
}

// NewSessionStore ...
func NewSessionStore(tnt *tarantool.Connection) *SessionStore {
	return &SessionStore{tnt}
}

// GetUserID ...
func (s *SessionStore) GetUserID(sid SessionID) (string, error) {
	resp, err := s.tnt.Call("get_user_id", []interface{}{sid})
	if err != nil {
		return "", fmt.Errorf("failed to call tarantool: %s", err)
	}

	if resp.Error != "" || resp.Code != 0 {
		return "", fmt.Errorf("failed to get user id: code=%d err=%q", resp.Code, resp.Error)
	}

	if len(resp.Data) != 1 {
		return "", fmt.Errorf("failed to get user id: empty data")
	}

	args, ok := resp.Data[0].([]interface{})
	if !ok || len(args) == 0 {
		return "", fmt.Errorf("failed to get user id: invalid data")
	}

	//user not found
	if args[0] == nil {
		return "", ErrNotFound
	}

	uid, ok := args[0].(string)
	if !ok {
		return "", fmt.Errorf("failed to get user id: invalid args")
	}

	return uid, nil
}

// ListSessionIDs ...
func (s *SessionStore) ListSessionIDs(uid string) ([]SessionID, error) {
	resp, err := s.tnt.Call("list_sids", []interface{}{uid})
	if err != nil {
		return nil, fmt.Errorf("failed to call tarantool: %s", err)
	}

	if resp.Error != "" || resp.Code != 0 {
		return nil, fmt.Errorf("failed to get session ids: code=%d err=%q", resp.Code, resp.Error)
	}

	if len(resp.Data) != 1 {
		return nil, fmt.Errorf("failed to get user id: empty data")
	}

	args, ok := resp.Data[0].([]interface{})
	if !ok {
		return nil, fmt.Errorf("failed to get user id: invalid data")
	}

	sessionIDs := make([]SessionID, 0, len(args))
	for _, a := range args {
		sid, ok := a.(string)
		if !ok {
			return nil, fmt.Errorf("failed to get user id: invalid data")
		}
		sessionIDs = append(sessionIDs, SessionID(sid))
	}

	return sessionIDs, nil
}