package sessions

import (
	"errors"
	"net/url"
	"time"

	tarantool "github.com/tarantool/go-tarantool"
)

// GetTNT creates new tnt connection
func GetTNT(tntDsn string) (*tarantool.Connection, error) {
	if tntDsn == "" {
		return nil, errors.New("--tnt-url is not set")
	}
	tntURL, err := url.Parse(tntDsn)
	if err != nil {
		return nil, errors.New("failed to parse tarantool connection URL err " + err.Error())
	}
	tntPswd, _ := tntURL.User.Password()

	opts := tarantool.Opts{
		Timeout:   500 * time.Millisecond,
		Reconnect: 1 * time.Second,
		User:      tntURL.User.Username(),
		Pass:      tntPswd,
	}

	tnt, err := tarantool.Connect(tntURL.Host, opts)
	if err != nil {
		return nil, errors.New("failed to connect to tnt err=%s" + err.Error())
	}
	return tnt, nil
}

// CreateSession creates new session in given tarantool storage
func CreateSession(tnt *tarantool.Connection, uid string) (string, error) {

	resp, err := tnt.Call("create_session", []interface{}{uid})
	if err != nil {
		return "", err
	}

	if resp.Error != "" || resp.Code != 0 {
		return "", errors.New("response code " + string(resp.Code) + " error " + resp.Error)
	}

	if len(resp.Data) != 1 {
		return "", errors.New("empty response")
	}

	args, ok := resp.Data[0].([]interface{})
	if !ok || len(args) == 0 {
		return "", errors.New("failed to get sid: invalid data")
	}

	sid, ok := args[0].(string)
	if !ok {
		return "", errors.New("failed to get sid: invalid args")
	}

	return sid, nil
}
