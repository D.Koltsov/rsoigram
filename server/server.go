package server

import (
	"gitlab.com/D.Koltsov/rsoigram/router"

	"github.com/gin-gonic/gin"
)

func Setup() *gin.Engine {
	r := gin.Default()
	router.Initialize(r)
	return r
}
