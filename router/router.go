package router

import (
	"log"

	"github.com/gin-gonic/gin"
	"gitlab.com/D.Koltsov/rsoigram/controllers"
	"gitlab.com/D.Koltsov/rsoigram/lib"
	middleware "gitlab.com/D.Koltsov/rsoigram/middleware"
	"gitlab.com/D.Koltsov/rsoigram/sessions"
)

func Initialize(r *gin.Engine) {
	r.GET("/", controllers.APIEndpoints)

	tnt, err := sessions.GetTNT(lib.Cfg.TarantoolDSN)
	if err != nil {
		log.Fatalf("failed to connect to tnt: %s", err)
	}

	api := r.Group("")
	api.POST("/login", controllers.Login)
	api.Use(middleware.NewAuthMiddleware(tnt).Auth)
	{
		api.GET("/users", controllers.GetUsers)
		api.GET("/users/:id", controllers.GetUser)
		api.GET("/usersByLogin", controllers.GetUserByLogin)
		api.POST("/chats", controllers.CreateChat)
		api.GET("/chats/:id", controllers.GetChat)
		api.POST("/messages", controllers.CreateMessage)
		api.POST("/users/delete", controllers.DeleteUser)
		api.POST("/chats/list", controllers.ListChats)
		api.POST("/messages/list", controllers.GetMessagesByChatID)
		// api.GET("/messages/:id", controllers.GetMessage)
		// api.PUT("/messages/:id", controllers.UpdateMessage)ин Й	Йрич к3ъв=п	ЦЪ

		// api.DELETE("/messages/:id", controllers.DeleteMessage)
	}
}
