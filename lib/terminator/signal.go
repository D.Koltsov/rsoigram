package terminator

import (
	"context"
	"errors"
	"os"
	"os/signal"
	"syscall"
)

// Term ...
type Term chan os.Signal

// ErrTermSig ...
var ErrTermSig = errors.New("termination signal from OS")

// TermSignal ...
func TermSignal() Term {
	term := Term(make(chan os.Signal, 1))

	signal.Notify(term, syscall.SIGINT, syscall.SIGTERM)

	return term
}

// Wait ...
func (t Term) Wait(ctx context.Context) error {
	select {
	case <-t:
		return ErrTermSig
	case <-ctx.Done():
		return ctx.Err()
	}
}
