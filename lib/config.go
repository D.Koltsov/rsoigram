package lib

import (
	"flag"
	"net/http"
)

type Retry struct {
	Count int32 //accesed atomicaly
	Req   http.Request
}

type Config struct {
	Fs *flag.FlagSet

	MsgURL   string
	UsersURL string
	ChatsURL string
	AuthURL  string

	TarantoolDSN string

	DefaultQueue chan Retry
}

var Cfg Config

func InitConfig(fs *flag.FlagSet, c *Config) {
	fs.StringVar(&c.MsgURL, "messages-url", "http://localhost:8082", "address:port to messages service")
	fs.StringVar(&c.UsersURL, "users-url", "http://localhost:8081", "address:port to messages service")
	fs.StringVar(&c.ChatsURL, "chats-url", "http://localhost:8080", "address:port to messages service")

	fs.StringVar(&c.AuthURL, "auth-url", "http://localhost:8083", "address:port to auth service")

	fs.StringVar(&c.TarantoolDSN, "tarantool dsn", "tarantool://htc:htc@127.0.0.1:3301", "address:port to messages service")
}
